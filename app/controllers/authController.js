const passport = require('passport');
const jwt = require('jsonwebtoken');
var path = require("path");
var env = process.env.NODE_ENV || "development";
var User = require ("../models").user

const config = require(path.join(__dirname, '..', 'config', 'config.json'))[env];

var exports = module.exports = {}


exports.authSignup = (req, res, next) => {
    // call for passport authentication
    passport.authenticate('local-signup', (err, user, info) => {
        // error from passport middleware
        if (err) return res.status(404).json(err);
        // registered user
        if (user) return res.status(200).json(user);
        // unknown user or wrong password
        else return res.status(401).json(info);
    })(req, res);
}

exports.authSignin = (req, res, next) => {
    // call for passport authentication
    passport.authenticate('local-signin', (err, user, info) => {
        // error from passport middleware
        if (err) return res.status(404).json(err);
        // registered user
        if (user) {
            let token = jwt.sign({
                _id: user.id
            }, config.JWT_SECRET,
                {
                    expiresIn: config.JWT_EXP
                });
            console.log("token", token)
            return res.status(200).json({ user, token: token });
        }
        // unknown user or wrong password
        else return res.status(401).json(info);
    })(req, res);
}

exports.getUser = async (req,res,next) =>{
    console.log(req.body.email,User)
    const user =await User.findOne({
        where: {
            email: req.body.email
        }
    });
    return res.status(200).json(user);
}
