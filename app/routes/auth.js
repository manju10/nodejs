var authController = require('../controllers/authController.js');
var passport   = require('passport')
const jwtHelper = require('../config/jwtHelper');

module.exports = function(app) {
 

    app.post('/signup', authController.authSignup);
    app.post('/signin',authController.authSignin);
    app.post('/userProfile', authController.getUser);
 

}